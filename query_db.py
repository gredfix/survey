from survey import Question, QuestionType, Text, \
    Rating, RatingList, Label, RatingLabel, \
    Select, SelectOption, Option, \
    Survey, Page, PageQuestion, SurveyResult, Answer, User

import pprint
from main import db
from datetime import datetime
import json


def create_survey() -> None:
    add_to_db(q_type)
    add_to_db(text)
    add_to_db(rating)
    add_to_db(select)
    add_to_db(label)
    add_to_db(option)
    add_to_db(survey)
    db.session.flush()

    add_to_db(page)
    db.session.flush()

    add_to_db(page_question)
    add_to_db(rating_label)
    add_to_db(rating_list)
    add_to_db(select_option)


def create_answer() -> None:
    add_to_db(user)
    add_to_db(survey_result)
    add_to_db(answers)
    db.session.commit()


def add_to_db(query: list) -> None:
    for el in query:
        db.session.add(el)


def get_full_survey_info(survey_id:int) -> dict:
    """
    Получить полную информацию по опросу
    :param survey_id: id опроса
    :return: словарь с полной информацией по опросу
    """

    survey = db.session.query(Survey).filter(Survey.id == survey_id).one_or_none()
    survey_info = {'id': survey.id, 'pages': []}
    questions = []
    answers = []

    for page in survey.page:
        for sq in page.page_question:
            for answer in sq.answer:
                answers.append({
                    'user': answer.survey_result.user_id,
                    'value': json.loads(answer.value)
                })
            questions.append({
                'id': sq.question.id,
                'type': sq.question.question_type,
                'title': sq.question.title,
                'answers': answers
            })
        survey_info['pages'].append({'id': page.id, 'questions': questions})
    return survey_info


def get_pages_pagination(survey_id: int, user_id: int) -> dict:
    """
    Получение пагинации для пользователя по user_id
    :param survey_id: id опроса
    :param user_id: id пользователя
    :return:
    """

    survey_result = db.session.query(SurveyResult).filter(
        SurveyResult.survey_id == survey_id, SurveyResult.user_id == user_id
    ).one_or_none()

    # вопросы, на которые ответил пользователь
    completed_question = [answer.page_question_id for answer in survey_result.answer]

    # определение состояния страниц с вопросами на основе ответа пользователей
    pagination = {}
    for page in survey_result.survey.page:
        for pq in page.page_question:
            if pq.required and pq.id not in completed_question:
                pagination[page.order] = {'id': page.id, 'is_fulfilled': False}
                break
        else:
            pagination[page.order] = {'id': page.id, 'is_fulfilled': True}
    return pagination


# def get_last_page(survey_id, user_id):
#     """
#     Получить последний шаг пользователя
#     :param survey_id: id опроса
#     :param user_id: id пользователя
#     :return: шаг
#     """
#
#     survey_result = db.session.query(SurveyResult).filter(
#         SurveyResult.survey_id == survey_id,
#         SurveyResult.user_id == user_id
#     ).one_or_none()
#
#     max_page_id = 0
#
#     for answer in survey_result.answers:
#         page_id = answer.question.pages_question[0].page_id
#         if page_id > max_page_id:
#             max_page_id = page_id
#
#     return max_page_id


q_type = [
    QuestionType(type='text'),
    QuestionType(type='rating'),
    QuestionType(type='select')
]

text = [
    Text(title="Расскажите о себе"),
    Text(title="Как вам опрос?")
]

label = [
    Label(value='Да'),
    Label(value='Нет'),
    Label(value='Не знаю'),
]

rating = [
    Rating(title="Какую бы оценку вы себе поставили?", range=5),
    Rating(title="Оцените, насколько эти качества соответствуют вам", range=5),
    Rating(title="Вам нравится Python?", range=3)
]

rating_label = [
    RatingLabel(rating_id=5, label_id=1, order=1),
    RatingLabel(rating_id=5, label_id=2, order=2),
    RatingLabel(rating_id=5, label_id=3, order=3),
]

rating_list = [
    RatingList(rating_id=4, title="Доброта", order=1),
    RatingList(rating_id=4, title="Честность", order=2),
    RatingList(rating_id=4, title="Трудолюбие", order=3),
]

option = [
    Option(value="Гений"),
    Option(value="Плейбой"),
    Option(value="Миллионер"),
    Option(value="Филантроп"),
    Option(value="Звёздные войны"),
    Option(value="Властелин колец"),
    Option(value="Гарри Поттер"),
    Option(value="Матрица"),
]

select = [
    Select(title="Выберите слово, наиболее характеризующее вас?", multiselect=False),
    Select(title="Выберите ваши любимые фильмы", multiselect=True)
]

select_option = [
    SelectOption(select_id=6, option_id=1),
    SelectOption(select_id=6, option_id=2),
    SelectOption(select_id=6, option_id=3),
    SelectOption(select_id=6, option_id=4),
    SelectOption(select_id=7, option_id=5),
    SelectOption(select_id=7, option_id=6),
    SelectOption(select_id=7, option_id=7),
    SelectOption(select_id=7, option_id=8),
]

survey = [
    Survey(title="Опрос 1")
]

user = [
    User(login="admin", name="Герман", surname="Греф"),
    User(login="admin", name="Никита", surname="Нужденко")
]

page = [
    Page(survey_id=1, order=2),
    Page(survey_id=1, order=1),
    Page(survey_id=1, order=3),
]

page_question = [
    PageQuestion(page_id=1, question_id=1, required=True),
    PageQuestion(page_id=1, question_id=3, required=True),
    PageQuestion(page_id=1, question_id=6, required=True),
    PageQuestion(page_id=2, question_id=4, required=True),
    PageQuestion(page_id=2, question_id=7, required=True),
    PageQuestion(page_id=2, question_id=5, required=True),
    PageQuestion(page_id=3, question_id=2),
]

survey_result = [
    SurveyResult(survey_id=1, user_id=1, date=datetime(2021, 12, 14, 10, 30), completed=True),
    SurveyResult(survey_id=1, user_id=2, date=datetime(2020, 10, 14, 10, 30), completed=False),
]

answers = [
    Answer(page_question_id=1, survey_result_id=1, value=json.dumps('Ну я главный тут у вас')),
    Answer(page_question_id=2, survey_result_id=1, value=json.dumps([5])),
    Answer(page_question_id=3, survey_result_id=1, value=json.dumps(['Гений'])),
    Answer(page_question_id=4, survey_result_id=1, value=json.dumps([5, 5, 5])),
    Answer(page_question_id=5, survey_result_id=1, value=json.dumps(['Гарри Поттер', 'Матрица'])),
    Answer(page_question_id=6, survey_result_id=1, value=json.dumps(['Нет'])),
    Answer(page_question_id=7, survey_result_id=1, value=json.dumps('Просто шикарно')),

    Answer(page_question_id=1, survey_result_id=2, value=json.dumps('Мега хакер на питоне')),
    Answer(page_question_id=2, survey_result_id=2, value=json.dumps([4])),
    Answer(page_question_id=3, survey_result_id=2, value=json.dumps(['Гений'])),
    Answer(page_question_id=4, survey_result_id=2, value=json.dumps([4, 4, 4])),
]

if __name__ == "__main__":
    # db.drop_all()     # удаляет все таблицы в бд
    # db.create_all()   # создает таблицы в бд
    # create_survey()   # создает опрос
    # create_answer()   # создает ответы

    pprint.pprint(get_full_survey_info(survey_id=1))
    print(get_pages_pagination(survey_id=1, user_id=2))
