from main import db


class Survey(db.Model):
    """
    Класс модели таблицы опросов
    """

    __tablename__ = 'survey'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    description = db.Column(db.String)
    release = db.Column(db.String)
    active = db.Column(db.Boolean)
    started_at = db.Column(db.DateTime)
    finished_at = db.Column(db.DateTime)

    # связь один ко многим с таблицей SurveyResult
    survey_result = db.relationship('SurveyResult', backref='survey', lazy=True)

    # связь один ко многим с таблицей Page
    page = db.relationship('Page', backref='survey', lazy=True)


class Page(db.Model):
    """
    Класс модели таблицы страниц опросов
    """

    __tablename__ = 'page'
    id = db.Column(db.Integer, primary_key=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    order = db.Column(db.Integer)
    title = db.Column(db.String)

    # связь один ко многим с таблицей PageQuestion
    page_question = db.relationship('PageQuestion', backref='page', lazy=True)


class PageQuestion(db.Model):
    """
    Класс модели таблицы связки страниц и вопросов
    """

    __tablename__ = 'page_question'
    id = db.Column(db.Integer, primary_key=True)
    page_id = db.Column(db.Integer, db.ForeignKey('page.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    order = db.Column(db.Integer)
    required = db.Column(db.Boolean)
    is_favorite = db.Column(db.Boolean)
    use_in_result = db.Column(db.Boolean)

    # связь один ко многим с таблицей Answer
    answer = db.relationship('Answer', backref='page_question', lazy=True)



class Answer(db.Model):
    """
    Класс модели таблицы ответов
    """

    __tablename__ = 'answer'
    page_question_id = db.Column(db.Integer, db.ForeignKey('page_question.id'), primary_key=True)
    survey_result_id = db.Column(db.Integer, db.ForeignKey('survey_result.id'), primary_key=True)
    value = db.Column(db.JSON)
    consider_weight = db.Column(db.Boolean)
    weight = db.Column(db.Integer)
    time = db.Column(db.DateTime)
    use_in_result = db.Column(db.Boolean)


class SurveyResult(db.Model):
    """
    Класс модели таблицы прохождения опросов
    """

    __tablename__ = 'survey_result'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    value = db.Column(db.JSON)
    date = db.Column(db.DateTime)
    completed = db.Column(db.Boolean)
    last_completed_page = db.Column(db.Integer)
    completed_at = db.Column(db.DateTime)
    description = db.Column(db.Text)

    # связь один ко многим с таблицей Answer
    answer = db.relationship('Answer', backref='survey_result', lazy=True)


class User(db.Model):
    """
    Класс модели таблицы пользователей
    """
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String)
    name = db.Column(db.String)
    surname = db.Column(db.String)
    father_name = db.Column(db.String)

    # связь один ко многим с таблицей SurveyResult
    survey_result = db.relationship('SurveyResult', backref='user', lazy=True)


class QuestionType(db.Model):
    """
    Класс модели типов комментируемых сущностей
    """

    __tablename__ = 'question_type'
    type = db.Column(db.String(20), primary_key=True)
    is_read = db.Column(db.Boolean)

    # связь один ко многим с таблицей Question
    question = db.relationship('Question', backref='type', lazy=True)


class Question(db.Model):
    """
    Класс модели таблицы комментируемых сущностей
    """

    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True)
    question_type = db.Column(db.String(20), db.ForeignKey('question_type.type'), nullable=False)

    # связь один ко многим с таблицей PageQuestion
    page_question = db.relationship('PageQuestion', backref='question', lazy=True)

    # аргументы mapper для создания полиморфизма по столбцу question_type
    __mapper_args__ = {
        'polymorphic_identity': 'question',
        'polymorphic_on': question_type
    }


class Text(Question):
    """
    Класс модели таблицы текстовых вопросов
    """

    __tablename__ = 'text'
    id = db.Column(db.Integer, db.ForeignKey('question.id', ondelete='CASCADE'), primary_key=True)
    title = db.Column(db.String)

    # аргумент mapper для подключения к полиморфизму таблицы Question
    # к столбцу question_type со значением 'text'
    __mapper_args__ = {
        'polymorphic_identity': 'text',
    }


class Label(db.Model):
    """
    Класс модели таблицы подписей значений рейтиноговых вопросов
    """
    __tablename__ = 'label'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)

    # связь один ко многим с таблицей RatingLabel
    rating_label = db.relationship('RatingLabel', cascade='all,delete', backref='label', lazy=True)


class RatingLabel(db.Model):
    """
    Класс модели таблицы связки рейтинговых вопросов с подписями
    """
    __tablename__ = 'rating_label'
    rating_id = db.Column(db.Integer, db.ForeignKey('rating.id', ondelete='CASCADE'), primary_key=True)
    label_id = db.Column(db.Integer, db.ForeignKey('label.id', ondelete='CASCADE'), primary_key=True)
    order = db.Column(db.Integer)
    weight = db.Column(db.Integer)


class RatingList(db.Model):
    """
    Класс модели таблицы группы рейтингов (мультирейтинг)
    """
    __tablename__ = 'rating_list'
    id = db.Column(db.Integer, primary_key=True)
    rating_id = db.Column(db.Integer, db.ForeignKey('rating.id', ondelete='CASCADE'))
    title = db.Column(db.String)
    order = db.Column(db.Integer)


class Rating(Question):
    """
    Класс модели таблицы рейтинговых вопросов
    """

    __tablename__ = 'rating'
    id = db.Column(db.Integer, db.ForeignKey('question.id', ondelete='CASCADE'), primary_key=True)
    title = db.Column(db.String)
    range = db.Column(db.Integer)

    # связь один ко многим с таблицей RatingList
    rating_list = db.relationship('RatingList', cascade='all,delete', backref='rating', lazy=True)

    # связь один ко многим с таблицей RatingLabel
    rating_label = db.relationship('RatingLabel', cascade='all,delete', backref='rating', lazy=True)

    # аргумент mapper для подключения к полиморфизму таблицы Question
    # к столбцу question_type со значением 'rating'
    __mapper_args__ = {
        'polymorphic_identity': 'rating',
    }


class Select(Question):
    """
    Класс модели таблицы селект-вопросов
    """

    __tablename__ = 'select'
    id = db.Column(db.Integer, db.ForeignKey('question.id', ondelete='CASCADE'), primary_key=True)
    title = db.Column(db.String)
    multiselect = db.Column(db.Boolean)

    # связь один ко многим с таблицей SelectOption
    select_option = db.relationship('SelectOption', cascade='all,delete', backref='select', lazy=True)

    # аргумент mapper для подключения к полиморфизму таблицы Question
    # к столбцу question_type со значением 'select'
    __mapper_args__ = {
        'polymorphic_identity': 'select',
    }


class Option(db.Model):
    """
    Класс модели таблицы вариантов ответа для селект-вопросов
    """
    __tablename__ = 'option'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)

    # связь один ко многим с таблицей SelectOption
    select_option = db.relationship('SelectOption', cascade='all,delete', backref='option', lazy=True)


class SelectOption(db.Model):
    """
    Класс модели таблицы SelectOption
    """
    __tablename__ = 'select_option'
    select_id = db.Column(db.Integer, db.ForeignKey('select.id', ondelete='CASCADE'), primary_key=True)
    option_id = db.Column(db.Integer, db.ForeignKey('option.id', ondelete='CASCADE'), primary_key=True)
    weight = db.Column(db.Integer)
