from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:host_lox@localhost/postgres?client_encoding=utf8'
db = SQLAlchemy(app)


if __name__ == "__main__":
    app.run(host='127.0.0.1')
